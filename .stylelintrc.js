// .stylelintrc.js
const { stylelint } = require("@factorial/stack-css");
const deepMerge = require("deepmerge");

module.exports = deepMerge(stylelint, {
  rules: {
    "comment-empty-line-before": ["always", { except: ["first-nested"] }],
  },
});
