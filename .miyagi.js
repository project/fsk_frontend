module.exports = {
  components: {
    folder: "templates",
  },
  engine: {
    name: "twig",
    options: {
      namespaces: {},
    },
  },
  extensions: [require("@miyagi/twig-drupal")],
  files: {
    css: {
      name: "<component>",
    },
    info: {
      extension: "yaml",
    },
    js: {
      name: "<component>",
    },
    mocks: {
      extension: "yaml",
    },
    templates: {
      extension: "html.twig",
      name: "<component>",
    },
    schema: {
      extension: "yaml",
    },
  },
};
